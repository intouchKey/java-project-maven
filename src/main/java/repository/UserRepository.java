package repository;

import DatabaseModel.UserInfo;
import app.Network;
import model.UserInfoModel;
import org.javalite.activejdbc.DB;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

public class UserRepository {
    public static UserInfoModel getInfo(String username) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (User Repo) <<");
            }

            UserInfo userInfoFromDatabase = UserInfo.findFirst("username = ?", username);

            UserInfoModel userInfo = new UserInfoModel();

            userInfo.setUsername(userInfoFromDatabase.get("username").toString());
            userInfo.setExperience((int) userInfoFromDatabase.get("experience"));
            userInfo.setGamesPlayed((int) userInfoFromDatabase.get("games_played"));
            userInfo.setLevel((int) userInfoFromDatabase.get("level"));
            userInfo.setCurrent_game_points((int) userInfoFromDatabase.get("current_game_points"));

            return userInfo;
        } catch (Exception e) {
            System.out.println(e.getMessage());

            return null;
        }
    }

    public static ArrayList<String> getMultipleInfo(String usernames) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (User Repo) <<");
            }

            String usernameQuery = "select * from user_infos where username = '" + usernames
                    .substring(0, usernames.length() - 1)
                    .replaceAll(",", "' OR username = '")
                    + "';";

            List<UserInfo> userInfoFromDatabase = UserInfo.findBySQL(usernameQuery);
            ArrayList<String> result = new ArrayList<String>();

            for (UserInfo userInfo: userInfoFromDatabase) {
                result.add(userInfo.toString());
            }

            return result;
        } catch (Exception e) {
            System.out.println(e.getMessage());

            return null;
        }
    }

    public static String updateInfo(String username, String level, String experience, String gamesPlayed, String currentGamePoints) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (User Repo) <<");
            }

            UserInfo userInfoFromDatabase = UserInfo.findFirst("username = ?", username);

            userInfoFromDatabase.set("experience", Integer.parseInt(experience));
            userInfoFromDatabase.set("games_played", Integer.parseInt(gamesPlayed));
            userInfoFromDatabase.set("level", Integer.parseInt(level));
            userInfoFromDatabase.set("current_game_points", Integer.parseInt(currentGamePoints));

            userInfoFromDatabase.saveIt();

            return "success";
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
