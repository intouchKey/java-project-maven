package repository;

import DatabaseModel.OngoingQuizzes;
import app.Network;
import model.DatasetModel;
import model.OngoingQuizzesModel;
import org.javalite.activejdbc.DB;

import java.security.SecureRandom;
import java.util.Dictionary;
import java.util.List;

public class OngoingQuizzesRepository {
    public static String initializeDatasets(List<DatasetModel> datasets, String roomName) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (OngoingQuizzes Repo) <<");
            }

            for (DatasetModel dataset : datasets) {
                OngoingQuizzes quiz = new OngoingQuizzes();
                quiz.set("room_name", roomName);
                quiz.set("question_set", dataset.getQuestion_set());
                quiz.set("question", dataset.getQuestion());
                quiz.set("id", generateSecureRandomString(10));
                quiz.set("correct_users", "");
                quiz.insert();
            }

            return "success";
        } catch ( Exception e ) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }

    public static String updateCorrectUsers(String roomName, String questionSet, String question, String username) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (OngoingQuizzes Repo) <<");
            }

            OngoingQuizzes ongoingQuiz = OngoingQuizzes.findFirst(
                    "room_name = ? and question_set = ? and question = ?",
                    roomName,
                    questionSet,
                    question
            );

            if (ongoingQuiz.get("correct_users").toString().equals("")) {
                ongoingQuiz.set("correct_users", username + ",");
            } else {
                ongoingQuiz.set(
                        "correct_users",
                        ongoingQuiz.get("correct_users").toString() + username + ","
                );
            }

            ongoingQuiz.saveIt();

            return "success";
        } catch ( Exception e ) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }

    public static String updateExecuted(String roomName, String questionSet, String question, String username) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (OngoingQuizzes Repo) <<");
            }

            OngoingQuizzes ongoingQuiz = OngoingQuizzes.findFirst(
                    "room_name = ? and question_set = ? and question = ?",
                    roomName,
                    questionSet,
                    question
            );

            ongoingQuiz.set(
                    "correct_users",
                    "true"
            );

            ongoingQuiz.saveIt();

            return "success";
        } catch ( Exception e ) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }

    public static OngoingQuizzesModel get(String roomName, String question, String questionSet) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (OngoingQuizzes Repo) <<");
            }

            System.out.println(roomName + " " + questionSet + " " + question);

            OngoingQuizzes ongoingQuiz = OngoingQuizzes.findFirst(
                    "room_name = ? and question_set = ? and question = ?",
                    roomName,
                    questionSet,
                    question
            );

            System.out.println(ongoingQuiz);

            OngoingQuizzesModel ongoingQuizzesToBeReturned = new OngoingQuizzesModel();
            ongoingQuizzesToBeReturned.setCorrect_users(ongoingQuiz.get("correct_users").toString());
            ongoingQuizzesToBeReturned.setExecuted(ongoingQuiz.get("executed").toString());

            return ongoingQuizzesToBeReturned;
        } catch ( Exception e ) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static String deleteQuizzes(String roomName) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (OngoingQuizzes Repo) <<");
            }

            List<OngoingQuizzes> ongoingQuizzes = OngoingQuizzes.find(
                    "room_name = ?",
                    roomName
            );

            for (OngoingQuizzes ongoingQuiz: ongoingQuizzes) {
                ongoingQuiz.delete();
            }

            return "success";
        } catch ( Exception e ) {
            System.out.println(e.getMessage());
            return "error";
        }
    }

    private static String generateSecureRandomString(int length) {
        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int rndCharAt = new SecureRandom().nextInt("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".length());
            char rndChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".charAt(rndCharAt);

            sb.append(rndChar);
        }

        return sb.toString();
    }
}
