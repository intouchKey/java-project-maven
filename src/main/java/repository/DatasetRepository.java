package repository;

import DatabaseModel.Dataset;
import app.Network;
import model.DatasetModel;
import org.javalite.activejdbc.DB;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

public class DatasetRepository {
    public static String create(DatasetModel dataset) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (Dataset Repo) <<");
            }

            int datasetCount = Dataset.count().intValue();
            String id = Integer.toString(datasetCount + 1);

            Dataset datasetToBeSaved = new Dataset();
            datasetToBeSaved.set("question_set", dataset.getQuestion_set());
            datasetToBeSaved.set("question", dataset.getQuestion());
            datasetToBeSaved.set("answers", dataset.getAnswers());
            datasetToBeSaved.set("correct_answer", dataset.getCorrect_answer());
            datasetToBeSaved.setId(id);

            datasetToBeSaved.insert();

            return "success";
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }

    public static String remove(String question) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (Dataset Repo) <<");
            }

            Dataset dataset = Dataset.findFirst("question = ?", question);

            if (dataset == null) {
                return "Dataset not found.";
            }

            dataset.delete();

            return "success";
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }

    public static List<DatasetModel> findByQuestionSet(String questionSet) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (Dataset Repo) <<");
            }

            List<Dataset> datasets = Dataset.where("question_set = '" + questionSet + "'");
            ArrayList<DatasetModel> datasetModels = new ArrayList<DatasetModel>();

            for (Dataset datasetFromDatabase: datasets) {
                DatasetModel dataset = new DatasetModel();
                dataset.setId(datasetFromDatabase.get("id").toString());
                dataset.setQuestion_set(datasetFromDatabase.get("question_set").toString());
                dataset.setQuestion(datasetFromDatabase.get("question").toString());
                dataset.setAnswers(datasetFromDatabase.get("answers").toString());
                dataset.setCorrect_answer(datasetFromDatabase.get("correct_answer").toString());

                datasetModels.add(dataset);
            }

            return datasetModels;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static List<DatasetModel> findAll() {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (Dataset Repo) <<");
            }

            List<Dataset> datasets = Dataset.findAll();
            ArrayList<DatasetModel> datasetModels = new ArrayList<DatasetModel>();

            for (Dataset datasetFromDatabase: datasets) {
                DatasetModel dataset = new DatasetModel();
                dataset.setId(datasetFromDatabase.get("id").toString());
                dataset.setQuestion_set(datasetFromDatabase.get("question_set").toString());
                dataset.setQuestion(datasetFromDatabase.get("question").toString());
                dataset.setAnswers(datasetFromDatabase.get("answers").toString());
                dataset.setCorrect_answer(datasetFromDatabase.get("correct_answer").toString());

                datasetModels.add(dataset);
            }

            return datasetModels;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static String createMultiple(ArrayList<DatasetModel> datasets) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (Dataset Repo) <<");

            }

            int datasetCount = Dataset.count().intValue();
            String id = Integer.toString(datasetCount + 1);

            for (DatasetModel dataset : datasets) {
                Dataset datasetToBeSaved = new Dataset();
                datasetToBeSaved.set("question_set", dataset.getQuestion_set());
                datasetToBeSaved.set("question", dataset.getQuestion());
                datasetToBeSaved.set("answers", dataset.getAnswers());
                datasetToBeSaved.set("correct_answer", dataset.getCorrect_answer());
                datasetToBeSaved.setId(id);

                datasetToBeSaved.insert();
                id = Integer.toString(Integer.parseInt(id) + 1);
            }

            return "success";
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }
}
