package repository;

import DatabaseModel.History;
import app.Network;
import model.HistoryModel;
import org.javalite.activejdbc.DB;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

public class HistoryRepository {
    public static String create(HistoryModel history) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (History Repo) <<");
            }

            History historyToBeSaved = new History();
            historyToBeSaved.set("username", history.getUsername());
            historyToBeSaved.set("question_set", history.getQuestion_set());
            historyToBeSaved.set("correct_answers_count", history.getCorrect_answers_count());
            historyToBeSaved.set("questions_count", history.getQuestions_count());
            historyToBeSaved.set("points", history.getPoints());
            historyToBeSaved.set("rank", history.getRank());
            historyToBeSaved.setId(generateSecureRandomString(8));

            historyToBeSaved.insert();

            return "success";
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }

    public static ArrayList<String> getByUsername(String username) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (History Repo) <<");
            }

            List<History> histories = History
                    .where("username = ?", username)
                    .limit(6)
                    .orderBy("created_at desc");

            ArrayList<String> result = new ArrayList<String>();

            for (History history: histories) {
                result.add(history.toString());
            }

            return result;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private static String generateSecureRandomString(int length) {
        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int rndCharAt = new SecureRandom().nextInt("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".length());
            char rndChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".charAt(rndCharAt);

            sb.append(rndChar);
        }

        return sb.toString();
    }
}
