package repository;

import DatabaseModel.Room;
import app.Network;
import model.RoomModel;
import org.javalite.activejdbc.DB;

import java.util.Dictionary;

public class RoomRepository {
    public static String create(RoomModel room) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (Room Repo) <<");
            }

            int roomCount = Room.count().intValue();
            String id = Integer.toString(roomCount + 1);

            Room roomToBeSaved = new Room();
            roomToBeSaved.set("name", room.getName());
            roomToBeSaved.set("password", room.getPassword());
            roomToBeSaved.setId(id);

            roomToBeSaved.insert();

            return "success";
        } catch ( Exception e ) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }

    public static boolean join(Room roomFromDatabase, String username) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (Room Repo) <<");
            }

            try {
                String membersString = roomFromDatabase.get("members").toString();
                roomFromDatabase.set("members", membersString + username + ",");
            } catch (Exception e) {
                roomFromDatabase.set("members", username + ",");
            }

            roomFromDatabase.saveIt();

            return true;
        } catch ( Exception e ) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public static Room find(String roomName) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (Room Repo) <<");
            }

            Room room = Room.findFirst("name = ?", roomName);

            return room;
        } catch ( Exception e ) {
            System.out.println(e.getMessage());

            return null;
        }
    }

    public static boolean updateQuestionSet(Room roomFromDatabase, String question_set) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (Room Repo) <<");
            }

            roomFromDatabase.set("dataset", question_set);

            roomFromDatabase.saveIt();

            return true;
        } catch ( Exception e ) {
            System.out.println("Error: " + e.getMessage());
            return false;
        }
    }

    public static boolean leave(Room roomFromDatabase, String removedMemberList) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (Room Repo) <<");
            }

            roomFromDatabase.set("members", removedMemberList);

            roomFromDatabase.saveIt();

            return true;
        } catch ( Exception e ) {
            System.out.println("Error: " + e.getMessage());
            return false;
        }
    }

    public static String startQuiz(Room roomFromDatabase) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (Room Repo) <<");
            }


            if (roomFromDatabase.get("dataset") == null) {
                return "No dataset.";
            }
            else if (roomFromDatabase.get("members") == null) {
                return "Not a single user has joined yet";
            }

            roomFromDatabase.set("quiz_started", true);
            roomFromDatabase.saveIt();

            return "success";
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
