package repository;

import DatabaseModel.Account;
import DatabaseModel.UserInfo;
import app.Network;
import model.AccountModel;
import org.javalite.activejdbc.DB;

import java.util.Dictionary;

import static java.lang.System.exit;

public class AccountRepository {
    public static boolean createAccount(AccountModel account) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                System.out.println(">> Connection pool (AccountRepo) <<");
            }

            int accountCount = Account.count().intValue();
            String id = Integer.toString(accountCount + 1);

            Account accountToBeSaved = new Account();
            accountToBeSaved.set("username", account.getUsername());
            accountToBeSaved.set("password", account.getPassword());
            accountToBeSaved.setId(id);

            accountToBeSaved.insert();

            UserInfo userInfoToBeSaved = new UserInfo();
            userInfoToBeSaved.set("username", account.getUsername());
            userInfoToBeSaved.set("level", 1);
            userInfoToBeSaved.setId(id);

            userInfoToBeSaved.insert();

            return true;
        } catch ( Exception e ) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public static AccountModel findAccount(String username) {
        try {
            Dictionary databaseInfo = Network.getDatabaseInfo();

            try {
                new DB(DB.DEFAULT_NAME).open(
                        "com.mysql.jdbc.Driver",
                        databaseInfo.get("dbUrl").toString(),
                        databaseInfo.get("username").toString(),
                        databaseInfo.get("password").toString());
            } catch (Exception e) {
                if (e.getMessage().substring(0, 29).equals("Failed to connect to JDBC URL")) {
                    System.out.println(">> Connection ERROR <<");
                    exit(0);
                } else {
                    System.out.println(">> Connection pool (Account Repo) <<");
                }
            }

            Account account = Account.findFirst("username = ?", username);

            AccountModel accountToBeReturned = new AccountModel();
            accountToBeReturned.setUsername(account.get("username").toString());
            accountToBeReturned.setPassword(account.get("password").toString());

            return accountToBeReturned;
        } catch ( Exception e ) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
