package app;

import com.mysql.jdbc.Connection;
import controller.*;
import org.javalite.activejdbc.DB;
import org.postgresql.jdbc2.optional.ConnectionPool;

import javax.sql.DataSource;
import java.util.Dictionary;
import java.util.StringTokenizer;

import static spark.Spark.*;


public class App {
    public static void main(String[] args) {
        Network.migrateDatabase();
        port(Network.getPort());

        path("", () -> {
            path("/call_database", () -> {
               get("/check", Network::establishConnection) ;
            });
            path("/authentication", () -> {
                get("/login", AuthenticationController::login);
                post("/register", AuthenticationController::createAccount);
            });
            path("/user", () -> {
               get("/get_info", UserController::getInfo);
               get("/get_multiple_info", UserController::getMultipleInfo);
               put("/update_info", UserController::updateInfo);
            });
            path("/room", () -> {
                post("/create", RoomController::create);
                put("/join", RoomController::join);
                put("/leave", RoomController::leave);
                get("/get_info", RoomController::getInfo);
                put("/update_question_set", RoomController::updateQuestionSet);
                post("/start_quiz", RoomController::startQuiz);
            });
            path("/dataset", () -> {
                post("/create", DatasetController::create);
                post("/create_multiple", DatasetController::createMultiple);
                delete("/remove", DatasetController::remove);
                get("/find_by_question_set", DatasetController::findByQuestionSet);
                get("/find_all", DatasetController::findAll);
            });
            path("/ongoing_quizzes", () -> {
                post("/start_quiz", OngoingQuizzesController::startQuiz);
                put("/update_correct_users", OngoingQuizzesController::updateCorrectUsers);
                put("/update_executed", OngoingQuizzesController::updateExecuted);
                get("/get_correct_users", OngoingQuizzesController::getCorrectUsers);
                get("/get_executed", OngoingQuizzesController::getExecuted);
                delete("/delete_quizzes", OngoingQuizzesController::deleteQuizByRoomName);
            });
            path("/history", () -> {
                post("/create", HistoryController::create);
                get("/get_by_username", HistoryController::getByUsername);
            });

            path("/*", () -> {
                get("", (req, res) -> {
                    return  JsonUtility.getApiText("GET");
                });
                post("", (req, res) -> {
                    return  JsonUtility.getApiText("POST");
                });
                put("", (req, res) -> {
                    return JsonUtility.getApiText("PUT");
                });
                delete("", (req, res) -> {
                    return JsonUtility.getApiText("DELETE");
                });
            });
        });
    }
}
