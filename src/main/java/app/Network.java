package app;

import org.flywaydb.core.Flyway;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.DB;
import spark.Request;
import spark.Response;

import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Dictionary;
import java.util.Hashtable;

import static java.lang.System.exit;

public class Network {
    public static String establishConnection(Request req, Response res) {
        Dictionary databaseInfo = Network.getDatabaseInfo();

        try {
            new DB(DB.DEFAULT_NAME).open(
                    "com.mysql.jdbc.Driver",
                    databaseInfo.get("dbUrl").toString(),
                    databaseInfo.get("username").toString(),
                    databaseInfo.get("password").toString());
        } catch (Exception e) {
            if (e.getMessage().substring(0, 29).equals("Failed to connect to JDBC URL")) {
                System.out.println(">> Connection ERROR <<");
                res.status(400);
                exit(0);
            } else {
                System.out.println(">> Connection pool (Network) <<");
            }
        }

        return "success";
    }

    public static void migrateDatabase() {
        try {
            Dictionary databaseInfo = getDatabaseInfo();

            Flyway flyway = Flyway.configure().dataSource(
                    databaseInfo.get("dbUrl").toString(),
                    databaseInfo.get("username").toString(),
                    databaseInfo.get("password").toString()
            ).load();

            flyway.migrate();
        } catch ( Exception e ) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            exit(0);
        }
    }

    public static int getPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 3333;
    }

    public static Dictionary getDatabaseInfo() {
        try {
            URI dbUri = new URI(System.getenv("DATABASE_URL"));

            Dictionary databaseInfo = new Hashtable();

            databaseInfo.put("username", dbUri.getUserInfo().split(":")[0]);
            databaseInfo.put("password", dbUri.getUserInfo().split(":")[1]);
            databaseInfo.put("dbUrl", "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath() + "?sslmode=require");

            return databaseInfo;
        } catch ( Exception e ) {
            Dictionary databaseInfo = new Hashtable();

            databaseInfo.put("username", "intouch");
            databaseInfo.put("password", "");
            databaseInfo.put("dbUrl", "jdbc:postgresql://localhost:5432/javabackend");

            return databaseInfo;
        }
    }
}
