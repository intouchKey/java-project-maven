package app;

import com.google.gson.Gson;

public class JsonUtility {
    public static String getApiText(String method) {
        return "<br/><div align=\"center\"><font face = \"Comic sans MS\"color=\"red\"size=\"20\">Welcome to Math Challenge APIs,<br/>" +
                "put in correct paths to use APIs<br/><br/></font></div>" +
                "<div align=\"center\"><font face = \"Bedrock\"color=\"black\"size=\"5\">Call Method: " + method + "<br/></font></div>" +
                "<div align=\"center\"><font face = \"Bedrock\"color=\"green\"size=\"5\">Website Status: " + "Online" + "<br/></font></div>" +
                "<font face = \"Bedrock\"color=\"blue\"size=\"5\"><br/>/authentication<br/>" +
                "    --> (GET) login<br/>" +
                "    --> (POST) register<br/>" +
                "<br/>/user<br/>" +
                "     --> (GET) get_info<br/>" +
                "     --> (GET) get_multiple_info<br/>" +
                "     --> (PUT) update_info<br/>" +
                "<br/>/room<br/>" +
                "     --> (POST) create<br/>" +
                "     --> (PUT) join<br/>" +
                "     --> (PUT) leave<br/>" +
                "     --> (GET) get_info<br/>" +
                "     --> (PUT) update_question_set<br/>" +
                "     --> (POST) start_quiz<br/>" +
                "<br/>/dataset<br/>" +
                "     --> (POST) create<br/>" +
                "     --> (POST) create_multiple<br/>" +
                "     --> (DELETE) remove<br/>" +
                "     --> (GET) find_by_question_set<br/>" +
                "     --> (GET) find_all<br/>" +
                "<br/>/ongoing_quizzes<br/>" +
                "     --> (POST) start_quiz<br/>" +
                "     --> (PUT) update_correct_users<br/>" +
                "     --> (PUT) update_executed<br/>" +
                "     --> (GET) get_correct_users<br/>" +
                "     --> (GET) get_executed<br/>" +
                "     --> (DELETE) delete_quizzes<br/>" +
                "<br/>/history<br/>" +
                "     --> (POST) create<br/>" +
                "     --> (GET) get_by_username<br/>"
                ;
    }

    public static String toJson(Object object) {
        return new Gson().toJson(object);
    }
}
