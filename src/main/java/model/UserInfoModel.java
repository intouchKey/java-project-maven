package model;

public class UserInfoModel {
    private String username;
    private int level;
    private int experience;
    private int games_played;
    private int current_game_points;

    public void setUsername(String username) {
        this.username = username;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.games_played = gamesPlayed;
    }

    public void setCurrent_game_points(int current_game_points) {
        this.current_game_points = current_game_points;
    }
}
