package model;

public class OngoingQuizzesModel {
    private String id;
    private String room_name;
    private String question_set;
    private String question;
    private String correct_users;
    private String executed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getQuestion_set() {
        return question_set;
    }

    public void setQuestion_set(String question_set) {
        this.question_set = question_set;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCorrect_users() {
        return correct_users;
    }

    public void setCorrect_users(String correct_users) {
        this.correct_users = correct_users;
    }

    public String getExecuted() {
        return executed;
    }

    public void setExecuted(String executed) {
        this.executed = executed;
    }
}
