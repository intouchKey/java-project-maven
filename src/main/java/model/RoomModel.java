package model;

public class RoomModel {
    private String name;
    private String password;
    private String members;
    private String dataset;
    private String quiz_started;

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public String getDataset() {
        return dataset;
    }

    public void setDataset(String dataset) {
        this.dataset = dataset;
    }

    public String isQuiz_started() {
        return quiz_started;
    }

    public void setQuiz_started(String quiz_started) {
        this.quiz_started = quiz_started;
    }
}
