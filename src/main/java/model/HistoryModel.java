package model;

public class HistoryModel {
    private String id;
    private String username;
    private String question_set;
    private String correct_answers_count;
    private String questions_count;
    private String points;
    private String rank;
    private String created_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getQuestion_set() {
        return question_set;
    }

    public void setQuestion_set(String question_set) {
        this.question_set = question_set;
    }

    public String getCorrect_answers_count() {
        return correct_answers_count;
    }

    public void setCorrect_answers_count(String correct_answers_count) {
        this.correct_answers_count = correct_answers_count;
    }

    public String getQuestions_count() {
        return questions_count;
    }

    public void setQuestions_count(String questions_count) {
        this.questions_count = questions_count;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "HistoryModel{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", question_set='" + question_set + '\'' +
                ", correct_answers_count='" + correct_answers_count + '\'' +
                ", questions_count='" + questions_count + '\'' +
                ", points='" + points + '\'' +
                ", rank='" + rank + '\'' +
                ", created_at='" + created_at + '\'' +
                '}';
    }
}
