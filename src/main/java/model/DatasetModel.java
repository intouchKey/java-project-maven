package model;

public class DatasetModel {
    private String id;
    private String question_set;
    private String question;
    private String answers;
    private String correct_answer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion_set() {
        return question_set;
    }

    public void setQuestion_set(String question_set) {
        this.question_set = question_set;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public String getCorrect_answer() {
        return correct_answer;
    }

    public void setCorrect_answer(String correct_answer) {
        this.correct_answer = correct_answer;
    }

    @Override
    public String toString() {
        return "DatasetModel{" +
                "id='" + id + '\'' +
                ", question_set='" + question_set + '\'' +
                ", question='" + question + '\'' +
                ", answers='" + answers + '\'' +
                ", correct_answer='" + correct_answer + '\'' +
                '}';
    }
}
