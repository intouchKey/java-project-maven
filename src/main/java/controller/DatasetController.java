package controller;

import com.google.gson.Gson;
import model.DatasetModel;
import repository.DatasetRepository;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class DatasetController {
    public static String create(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        DatasetModel dataset = new Gson().fromJson(req.body(), DatasetModel.class);

        String statusMessage = DatasetRepository.create(dataset);

        if (statusMessage != "success") {
            res.status(400);
            return statusMessage;
        }

        return "success";
    }

    public static String createMultiple(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String datasetsString = req.body();

        StringTokenizer st1 = new StringTokenizer(datasetsString, "|");

        ArrayList<DatasetModel> datasets = new ArrayList<DatasetModel>();

        for (int index = 1; st1.hasMoreTokens(); index++) {
            String eachDataSet = st1.nextToken();

            datasets.add(new Gson().fromJson(eachDataSet, DatasetModel.class));
        }

        String statusMessage = DatasetRepository.createMultiple(datasets);

        if (statusMessage != "success") {
            res.status(400);
            return statusMessage;
        }

        return "success";
    }

    public static String remove(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String question = req.headers("question");

        String statusMessage = DatasetRepository.remove(question);

        if (statusMessage != "success") {
            res.status(400);
            return statusMessage;
        }

        return statusMessage;
    }

    public static String findByQuestionSet(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String questionSet = req.headers("question_set");

        List<DatasetModel> datasetList = DatasetRepository.findByQuestionSet(questionSet);

        String payload = "";

        for (DatasetModel dataset: datasetList) {
            payload += dataset.toString().substring(12) + "|";
        }

        return payload.substring(0, payload.length() - 1);
    }

    public static String findAll(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        List<DatasetModel> datasetList = DatasetRepository.findAll();

        String payload = "";

        for (DatasetModel dataset: datasetList) {
            payload += dataset.toString().substring(12) + "|";
        }

        return payload.substring(0, payload.length() - 1);
    }
}
