package controller;

import app.JsonUtility;
import model.UserInfoModel;
import repository.UserRepository;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.Hashtable;

public class UserController {
    private static Hashtable<String, String> cache = new Hashtable<String, String>();
    private static Hashtable<String, String> cache2 = new Hashtable<String, String>();

    public static String getInfo(Request req, Response res) {
        String username = req.headers("username");

        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        try {
            String cacheResult = cache.get(username);

            if (cacheResult.equals("null") || cacheResult == null) {
                throw new Exception();
            }
            System.out.println(">> CACHE (User) -- HIT <<");
            return cacheResult;
        } catch (Exception e) {
            System.out.println(">> CACHE (User) -- MISS <<");
        }

        UserInfoModel userInfo = UserRepository.getInfo(username);

        String result = JsonUtility.toJson(userInfo);

        cache.put(username, result);

        return result;
    }

    public static String getMultipleInfo(Request req, Response res) {
        String usernames = req.headers("usernames");

        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        try {
            String cacheResult = cache2.get(usernames);

            if (cacheResult.equals("null") || cacheResult == null) {
                throw new Exception();
            }
            System.out.println(">> CACHE (User > Multiple <) -- HIT <<");
            return cacheResult;
        } catch (Exception e) {
            System.out.println(">> CACHE (User > Multiple <) -- MISS <<");
        }

        ArrayList<String> userInfos = UserRepository.getMultipleInfo(usernames);
        String result = "";

        for (String userinfo: userInfos) {
            result += userinfo.substring(64);
            result += '|';
        }

        cache2.put(usernames, result);

        return result;
    }

    public static String updateInfo(Request req, Response res) {
        String username = req.headers("username");
        String level = req.headers("level");
        String experience = req.headers("experience");
        String gamesPlayed = req.headers("games_played");
        String currentGamePoints = req.headers("current_game_points");

        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String status = UserRepository.updateInfo(username, level, experience, gamesPlayed, currentGamePoints);

        if (status != "success") {
            res.status(400);

            return status;
        }

        try {
            cache.put(username, "null");
            cache2.clear();
        } catch (Exception e) {}

        return "success";
    }
}
