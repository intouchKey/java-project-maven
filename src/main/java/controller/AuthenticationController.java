package controller;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.gson.Gson;
import model.AccountModel;
import repository.AccountRepository;
import spark.Request;
import spark.Response;

import java.util.Date;
import java.util.Hashtable;

public class AuthenticationController {
    private static Hashtable<String, AccountModel> cache = new Hashtable<String, AccountModel>();

    public static String createAccount(Request req, Response res) {
        AccountModel account = new Gson().fromJson(req.body(), AccountModel.class);

        boolean validateSuccess = validate(account);

        if (!validateSuccess) {
            res.status(400);
            return "Validation Error";
        }

        String encryptedPassword = BCrypt.withDefaults().hashToString(12, account.getPassword().toCharArray());
        account.setPassword(encryptedPassword);

        String token = generateToken();

        boolean status = AccountRepository.createAccount(account);

        if (status == false) {
            res.status(400);
            return "Account Taken";
        }

        return token;
    }

    public static String login(Request req, Response res) {
        AccountModel account = new Gson().fromJson(req.body(), AccountModel.class);

        String token = generateToken();

        AccountModel accountFromDatabase;

        try {
            AccountModel cacheResult = cache.get(account.getUsername());

            if (cacheResult.equals("null") || cacheResult == null) {
                throw new Exception();
            }
            System.out.println(">> CACHE (Authentication) -- HIT <<");

            accountFromDatabase = cacheResult;
        } catch (Exception e) {
            System.out.println(">> CACHE (Authentication) -- MISS <<");
            accountFromDatabase = AccountRepository.findAccount(account.getUsername());
        }

        if (accountFromDatabase == null) {
            res.status(500);
            return "Account is not registered yet";
        }

        BCrypt.Result result = BCrypt.verifyer().verify(
                account.getPassword().toCharArray(),
                accountFromDatabase.getPassword().toCharArray()
        );

        if (result.verified == false) {
            res.status(400);
            return "Authentication Error";
        }

        cache.put(account.getUsername(), accountFromDatabase);

        return token;
    }

    private static String generateToken() {
        try {
            Algorithm algorithm = Algorithm.HMAC256("secretkey");
            long currentTimeInMilliseconds = System.currentTimeMillis();
            Date now = new Date(currentTimeInMilliseconds);
            long expireTimeInMilliseconds = currentTimeInMilliseconds + 7200000;
            Date expire = new Date(expireTimeInMilliseconds);

            String token = JWT.create()
                    .withIssuer("MathChallenge")
                    .withIssuedAt(now)
                    .withExpiresAt(expire)
                    .sign(algorithm);

            return token;
        } catch (JWTCreationException exception){
            return "Error while creating account";
        }
    }

    private static boolean validate(AccountModel account) {
        if (
                        account.getUsername().length() > 0
                        && account.getUsername().length() <= 20
                        && account.getPassword().length() >= 6
                        && account.getPassword().length() <= 20
                        && hasDigit(account.getPassword())
                        && hasCharacter(account.getPassword())
        ) {
            return true;
        }

        return false;
    }

    private static boolean hasDigit(String text) {
        for (int i = 0; i < text.length(); i++) {
            if (Character.isDigit(text.charAt(i))) {
                return true;
            }
        }

        return false;
    }

    private static boolean hasCharacter(String text) {
        for (int i = 0; i < text.length(); i++) {
            if (Character.isAlphabetic(text.charAt(i))) {
                return true;
            }
        }

        return false;
    }

    public static boolean verifyTokenByHeader(String bearerToken) {
        try {
            String token = bearerToken.split(" ")[1];
            Algorithm algorithm = Algorithm.HMAC256("secretkey");
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("MathChallenge")
                    .acceptExpiresAt(5)
                    .build();

            DecodedJWT jwt = verifier.verify(token);

            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
