package controller;

import DatabaseModel.Room;
import app.JsonUtility;
import at.favre.lib.crypto.bcrypt.BCrypt;
import com.google.gson.Gson;
import model.RoomModel;
import repository.RoomRepository;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class RoomController {
    private static Hashtable<String, String> cache = new Hashtable<String, String>();

    public static String create(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        RoomModel room = new Gson().fromJson(req.body(), RoomModel.class);

        boolean validateSuccess = validate(room);

        if (!validateSuccess) {
            res.status(400);
            return "Validation Error";
        }

        String encryptedPassword = BCrypt.withDefaults().hashToString(12, room.getPassword().toCharArray());
        room.setPassword(encryptedPassword);

        String statusMessage = RoomRepository.create(room);

        if (statusMessage != "success") {
            res.status(400);
            System.out.println(statusMessage);

            return "Name taken";
        }

        return statusMessage;
    }

    public static String join(Request req, Response res) {
        RoomModel room = new Gson().fromJson(req.body(), RoomModel.class);

        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String username = req.headers("username");

        Room roomFromDatabase = RoomRepository.find(room.getName());

        if (roomFromDatabase == null) {
            res.status(500);
            return "Room is not registered";
        }

        BCrypt.Result result = BCrypt.verifyer().verify(
                room.getPassword().toCharArray(),
                roomFromDatabase.get("password").toString().toCharArray()
        );

        if (result.verified == false) {
            res.status(400);
            return "Authentication Error";
        }

        try {
            String members = roomFromDatabase.get("members").toString();

            StringTokenizer st1 = new StringTokenizer(members, ",");

            for (int index = 1; st1.hasMoreTokens(); index++) {
                String membersToken = st1.nextToken();

                if (username.equals(membersToken)) {
                    res.status(400);
                    return "User already registered";
                }
            }
        } catch (Exception e) { }

        boolean status = RoomRepository.join(roomFromDatabase, username);

        if (status == false) {
            res.status(400);
            return "Internal Server Error";
        }

        try {
            cache.put(room.getName(), "null");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return "success";
    }

    public static String leave(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String username = req.headers("username");
        String roomName = req.headers("room_name");

        Room roomFromDatabase = RoomRepository.find(roomName);

        if (roomFromDatabase == null) {
            res.status(500);
            return "Room is not registered";
        }

        String members;

        try {
            members = roomFromDatabase.get("members").toString();
        } catch (Exception e) {
            res.status(400);
            return "Room is empty.";
        }

        StringTokenizer st1 = new StringTokenizer(members, ",");

        ArrayList<String> membersList = new ArrayList<String>();

        boolean found = false;

        for (int index = 1; st1.hasMoreTokens(); index++) {
            String membersToken = st1.nextToken();

            if (username.equals(membersToken)) {
                found = true;
                continue;
            }

            membersList.add(membersToken);
        }

        if (found == false) {
            res.status(400);
            return "User has not joined room";
        }

        String removedMemberList = String.join(",", membersList) + ",";

        boolean status = RoomRepository.leave(roomFromDatabase, removedMemberList);

        if (status == false) {
            res.status(400);
            return "Internal Server Error";
        }

        try {
            cache.put(roomName, "null");
        } catch (Exception e) {}

        return "success";
    }

    public static String getInfo(Request req, Response res) {
        String roomName = req.headers("room_name");

        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        try {
            String cacheResult =  cache.get(roomName);

            if (cacheResult.equals("null") || cacheResult == null) {
                throw new Exception();
            }
            System.out.println(">> CACHE (Room) -- HIT <<");
            return cacheResult;
        } catch (Exception e) {
            System.out.println(">> CACHE (Room) -- MISS <<");
        }

        Room roomFromDatabase = RoomRepository.find(roomName);

        if (roomFromDatabase == null) {
            res.status(400);
            return "room not found";
        }

        RoomModel room = new RoomModel();
        room.setName(roomFromDatabase.get("name").toString());
        room.setDataset(roomFromDatabase.get("dataset").toString());
        room.setQuiz_started(roomFromDatabase.get("quiz_started").toString());

        try {
            room.setMembers(roomFromDatabase.get("members").toString());
        } catch (Exception e) {
            room.setMembers("");
        }

        String result = JsonUtility.toJson(room);

        cache.put(roomName, result);

        return result;
    }

    public static String updateQuestionSet(Request req, Response res) {
        String roomName = req.headers("room_name");
        String questionSet = req.headers("question_set");

        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        Room roomFromDatabase = RoomRepository.find(roomName);

        if (roomFromDatabase == null) {
            res.status(400);
            return "room not found";
        }

        boolean status = RoomRepository.updateQuestionSet(roomFromDatabase, questionSet);

        if (status == false) {
            res.status(400);
            return "Internal server error";
        }

        try {
            cache.put(roomName, "null");
        } catch (Exception e) {}

        return "success";
    }

    public static String startQuiz(Request req, Response res) {
        String roomName = req.headers("room_name");

        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        Room roomFromDatabase = RoomRepository.find(roomName);

        if (roomFromDatabase == null) {
            res.status(400);
            return "room not found";
        }

        String statusMessage = RoomRepository.startQuiz(roomFromDatabase);

        if (statusMessage != "success") {
            res.status(400);
            return statusMessage;
        }

        try {
            cache.put(roomName, "null");
        } catch (Exception e) {}

        return "success";
    }

    private static boolean validate(RoomModel room) {
        if (
                room.getName().length() > 0
                        && room.getName().length() <= 20
                        && room.getPassword().length() >= 6
                        && room.getPassword().length() <= 20
                        && hasDigit(room.getPassword())
                        && hasCharacter(room.getPassword())
        ) {
            return true;
        }

        return false;
    }

    private static boolean hasDigit(String text) {
        for (int i = 0; i < text.length(); i++) {
            if (Character.isDigit(text.charAt(i))) {
                return true;
            }
        }

        return false;
    }

    private static boolean hasCharacter(String text) {
        for (int i = 0; i < text.length(); i++) {
            if (Character.isAlphabetic(text.charAt(i))) {
                return true;
            }
        }

        return false;
    }
}
