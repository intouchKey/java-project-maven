package controller;

import DatabaseModel.Room;
import model.DatasetModel;
import model.OngoingQuizzesModel;
import repository.DatasetRepository;
import repository.OngoingQuizzesRepository;
import repository.RoomRepository;
import spark.Request;
import spark.Response;

import java.util.Hashtable;
import java.util.List;

public class OngoingQuizzesController {
    private static Hashtable<String, OngoingQuizzesModel> cache = new Hashtable<String, OngoingQuizzesModel>();

    public static String startQuiz(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String roomName = req.headers("room_name");

        Room roomFromDatabase = RoomRepository.find(roomName);

        if (roomFromDatabase == null) {
            return "Room not found";
        }

        Object questionSet = roomFromDatabase.get("dataset");

        if (questionSet == null) {
            return "Room has no dataset";
        }

        List<DatasetModel> datasets = DatasetRepository.findByQuestionSet(questionSet.toString());

        String statusMessage = OngoingQuizzesRepository.initializeDatasets(datasets, roomName);

        if (statusMessage != "success") {
            res.status(400);
            return statusMessage;
        }

        return "success";
    }

    public static String updateCorrectUsers(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String roomName = req.headers("room_name");
        String questionSet = req.headers("question_set");
        String question = req.headers("question");
        String username = req.headers("username");

        String statusMessage = OngoingQuizzesRepository.updateCorrectUsers(roomName, questionSet, question, username);

        if (statusMessage != "success") {
            res.status(400);
            return statusMessage;
        }

        try {
            OngoingQuizzesModel temp = new OngoingQuizzesModel();
            temp.setRoom_name("null");
            cache.put(roomName + question + questionSet, temp);
        } catch (Exception e) {}

        return "success";
    }

    public static String getCorrectUsers(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String roomName = req.headers("room_name");
        String question = req.headers("question");
        String questionSet = req.headers("question_set");

        try {
            OngoingQuizzesModel cacheResult =  cache.get(roomName + question + questionSet);

            if (cacheResult == null || cacheResult.getRoom_name().equals("null")) {
                throw new Exception();
            }

            System.out.println(">> CACHE (OngoingQuizzes > Correct users <) -- HIT <<");
            return cacheResult.getCorrect_users();
        } catch (Exception e) {
            System.out.println(">> CACHE (OngoingQuizzes > Correct users <) -- MISS <<");
        }

        OngoingQuizzesModel ongoingQuizzesModel = OngoingQuizzesRepository.get(roomName, question, questionSet);

        if (ongoingQuizzesModel == null) {
            res.status(400);
            return "Error while finding correct users";
        }

        try {
            OngoingQuizzesModel temp = new OngoingQuizzesModel();
            temp.setRoom_name("null");
            cache.put(roomName + question + questionSet, temp);
        } catch (Exception e) {}

        return ongoingQuizzesModel.getCorrect_users();
    }

    public static String getExecuted(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String roomName = req.headers("room_name");
        String question = req.headers("question");
        String questionSet = req.headers("question_set");

        try {
            OngoingQuizzesModel cacheResult =  cache.get(roomName + question + questionSet);

            if (cacheResult == null || cacheResult.getRoom_name().equals("null")) {
                throw new Exception();
            }

            System.out.println(">> CACHE (OngoingQuizzes > Correct users <) -- HIT <<");
            return cacheResult.getExecuted();
        } catch (Exception e) {
            System.out.println(">> CACHE (OngoingQuizzes > Correct users <) -- MISS <<");
        }

        OngoingQuizzesModel ongoingQuizzesModel = OngoingQuizzesRepository.get(roomName, question, questionSet);

        if (ongoingQuizzesModel == null) {
            res.status(400);
            return "Error while finding correct users";
        }

        cache.put(roomName + question + questionSet, ongoingQuizzesModel);

        return ongoingQuizzesModel.getExecuted();
    }

    public static String updateExecuted(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String roomName = req.headers("room_name");
        String questionSet = req.headers("question_set");
        String question = req.headers("question");
        String username = req.headers("username");

        String statusMessage = OngoingQuizzesRepository.updateExecuted(roomName, questionSet, question, username);

        if (statusMessage != "success") {
            res.status(400);
            return statusMessage;
        }

        try {
            OngoingQuizzesModel temp = new OngoingQuizzesModel();
            temp.setRoom_name("null");
            cache.put(roomName + question + questionSet, temp);
        } catch (Exception e) {}

        return "success";
    }

    public static String deleteQuizByRoomName(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        String roomName = req.headers("room_name");

        String statusMessage = OngoingQuizzesRepository.deleteQuizzes(roomName);

        if (statusMessage != "success") {
            res.status(400);
            return statusMessage;
        }

        cache.clear();

        return "success";
    }
}
