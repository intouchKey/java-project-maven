package controller;

import model.HistoryModel;
import repository.HistoryRepository;
import spark.Request;
import spark.Response;

import java.util.ArrayList;

public class HistoryController {
    public static String create(Request req, Response res) {
        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        HistoryModel history = new HistoryModel();
        history.setUsername(req.headers("username"));
        history.setQuestion_set(req.headers("question_set"));
        history.setCorrect_answers_count(req.headers("correct_answers_count"));
        history.setQuestions_count(req.headers("questions_count"));
        history.setPoints(req.headers("points"));
        history.setRank(req.headers("rank"));

        String statusMessage = HistoryRepository.create(history);

        if (statusMessage != "success") {
            res.status(400);
            return statusMessage;
        }

        return "success";
    }

    public static String getByUsername(Request req, Response res) {
        String username = req.headers("username");

        if (!AuthenticationController.verifyTokenByHeader(req.headers("Authorization"))) {
            res.status(400);
            return "Authentication Error";
        }

        ArrayList<String> histories = HistoryRepository.getByUsername(username);
        String result = "";

        for (String history: histories) {
            result += history.substring(60);
            result += '|';
        }

        return result;
    }
}
