DROP TABLE accounts;

CREATE TABLE accounts (
    username VARCHAR NOT NULL PRIMARY KEY,
    password VARCHAR NOT NULL,
    id VARCHAR NOT NULL
);