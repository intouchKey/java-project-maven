DROP TABLE accounts;

CREATE TABLE accounts (
    username VARCHAR NOT NULL,
    password VARCHAR NOT NULL,
    id VARCHAR NOT NULL PRIMARY KEY
);