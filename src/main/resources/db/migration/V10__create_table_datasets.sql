CREATE TABLE dataset (
    id VARCHAR NOT NULL,
    question_set VARCHAR NOT NULL PRIMARY KEY,
    question VARCHAR NOT NULL,
    answers VARCHAR NOT NULL,
    correct_answer VARCHAR NOT NULL
);