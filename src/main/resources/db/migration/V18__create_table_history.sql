CREATE TABLE history (
    id VARCHAR NOT NULL,
    question_set VARCHAR NOT NULL PRIMARY KEY,
    correct_answers_count VARCHAR NOT NULL,
    questions_count VARCHAR NOT NULL,
    points VARCHAR NOT NULL,
    rank VARCHAR NOT NULL
);