CREATE TABLE ongoing_quizzes (
    id VARCHAR NOT NULL PRIMARY KEY,
    room_name VARCHAR NOT NULL,
    question_set VARCHAR NOT NULL,
    question VARCHAR NOT NULL,
    correct_users VARCHAR NOT NULL
);