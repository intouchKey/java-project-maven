DROP TABLE datasets;

CREATE TABLE datasets (
    id VARCHAR NOT NULL PRIMARY KEY,
    question_set VARCHAR NOT NULL,
    question VARCHAR NOT NULL,
    answers VARCHAR NOT NULL,
    correct_answer VARCHAR NOT NULL
);