CREATE TABLE user_infos (
    username VARCHAR NOT NULL PRIMARY KEY,
    level INT DEFAULT 0,
    experience INT DEFAULT 0,
    games_played INT DEFAULT 0,
    id VARCHAR NOT NULL
);